Ansible Builder
========

ESS specific [Ansible Builder](https://github.com/ansible/ansible-builder) container image.

This image is configured to use ESS internal pypi index.
