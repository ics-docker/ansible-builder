FROM python:3

RUN apt-get -y update && apt-get install -y --no-install-recommends python3-venv

RUN pip config set global.index-url https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple && \
    pip config set global.trusted-host artifactory.esss.lu.se && \
    pip install --no-cache-dir --upgrade pip && \
    pip install ansible-builder
